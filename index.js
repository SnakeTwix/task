require('dotenv').config();
const { startServer } = require("./server");
const logger = require("./services/logger.service")(module);
// todo: разкомментировать для работы с БД
const { postgres } = require("./services/database.service");

(async () => {
  try {
    await postgres.connect();
    await postgres.runMigrations();
    startServer();
  } catch (error) {
    logger.error(error.message);
    await postgres.disconnect();
    logger.shutdown(() => process.exit(1));
  }
})();

["SIGINT", "SIGTERM", "SIGQUIT"].forEach((signal) =>
  process.on(signal, async () => {
    await postgres.disconnect();
    logger.info(`Caught signal ${signal}`);
    logger.shutdown(() => process.exit(0));
  })
);

process.on("uncaughtException", async (error) => {
  await postgres.disconnect();
  logger.error(`Uncaught exception! ${error.message}`);
  logger.shutdown(() => process.exit(1));
});
