function handleSync(func) {
  let result = null;
  let error = null;

  try {
    result = func()
  } catch (e) {
    error = e
  }

  return [result, error]
}

exports.handleSync = handleSync;