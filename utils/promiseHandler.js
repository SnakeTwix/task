async function handlePromise(promise) {
  let result = null;
  let error = null;

  try {
    result = await promise;
  } catch (e) {
    error = e
  }

  return [result, error]
}

exports.handlePromise = handlePromise