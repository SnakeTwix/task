const path = require("path");
const { body, check, query } = require("express-validator");
const { UnprocessableEntity } = require("../../constants/errors");
const validate = require("../../middleware/validation.middleware");
const logger = require("../../services/logger.service")(module);
const imageService = require("../../services/image.service");

const getOne = [
  check("id").isNumeric().withMessage({
    code: UnprocessableEntity,
    message: "id: parameter has incorrect format",
  }),
  validate,
];

const editOne = [
  check("id").isNumeric().withMessage({
    code: UnprocessableEntity,
    message: "id: parameter has incorrect format",
  }),
  validate,
];

const addImage = [
  check("id").isNumeric().withMessage({
    code: UnprocessableEntity,
    message: "id: parameter has incorrect format",
  }),
  body()
    .custom((_, { req }) => req.files?.file[0])
    .withMessage({
      code: UnprocessableEntity,
      message: "file: parameter is required",
    })
    .bail()
    .custom(async (_, { req }) => {
      const file = req.files.file[0];
      const fileExtension = path.extname(file.originalname).toLowerCase();
      const tempFilePath = file.path;

      const isAllowedExtension = [".png", ".jpg", ".jpeg", ".gif"].includes(
        fileExtension
      );
      if (!isAllowedExtension) {
        await imageService
          .removeImage(tempFilePath)
          .catch((err) => logger.error(err));
      }
      return isAllowedExtension;
    })
    .withMessage({
      code: UnprocessableEntity,
      message: "files.file: only image files are allowed to upload",
    }),
  validate,
];

const removeImage = [
  check("id").isNumeric().withMessage({
    code: UnprocessableEntity,
    message: "id: parameter has incorrect format",
  }),
  check("image_name")
    .notEmpty()
    .withMessage((_, { path }) => ({
      code: UnprocessableEntity,
      message: `${path}: parameter is required`,
    })),
  validate,
];

const postOne = [
  body("name")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "name: required"
    }),
  body("short_name")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "short_name: required"
    }),
  body("business_entity")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "business_entity: required"
    }),
  body("status")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "status: required"
    }),
  body("type")
    .isArray()
    .withMessage({
      code: UnprocessableEntity,
      message: "type: must be array"
    }),
  body("contract")
    .isObject()
    .withMessage({
      code: UnprocessableEntity,
      message: "contract: must be object"
    }),
  body("contact")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "contact: required"
    })
    .isNumeric()
    .withMessage({
      code: UnprocessableEntity,
      message: "contact: must be number"
    }),
  body("address")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "address: required"
    }),
  validate
]

const getMany = [
  query("limit")
    .isInt()
    .withMessage({
      code: UnprocessableEntity,
      message: "limit: should be a number"
    })
    .custom(value => parseInt(value) <= 100)
    .withMessage({
      code: UnprocessableEntity,
      message: "limit: Should be less than 101"
    }),
  query("page")
    .isInt()
    .withMessage({
      code: UnprocessableEntity,
      message: "page: should be a number"
    }),
  query("status")
    .optional(),
  query("type")
    .optional(),
  validate
]


module.exports = { getOne, editOne, addImage, removeImage, postOne, getMany };
