const logger = require("../../../services/logger.service")(module);
const { OK } = require("../../../constants/http-codes");
const companyMethods = require("../../../DB/postgres/methods/company");
const { getUrlForRequest } = require("../../../helpers/url.helper");
const { NotFound, InternalError } = require("../../../constants/errors");
const { parseOne } = require("../companies.service");
const { handlePromise } = require("../../../utils/promiseHandler");

/**
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 * @returns {Promise<void>}
 */
async function getMany(req, res) {
  logger.init("get company");
  let {limit, page, status, type} = req.query;
  const offset = page * limit;

  let [companies, readError] = await handlePromise(companyMethods.getMany(parseInt(limit), offset, status, type));
  if (readError) {
    console.log(readError)
    throw new InternalError();
  }
  const photoUrl = getUrlForRequest(req);
  companies = companies.map(company => parseOne(company, photoUrl))

  res.status(OK).json(companies);
  logger.success();
}

module.exports = {
  getMany,
};
