const logger = require("../../../services/logger.service")(module);
const { OK } = require("../../../constants/http-codes");
const companyMethods = require("../../../DB/postgres/methods/company");
const { getUrlForRequest } = require("../../../helpers/url.helper");
const { NotFound, InternalError } = require("../../../constants/errors");
const { parseOne } = require("../companies.service");
const { handlePromise } = require("../../../utils/promiseHandler");

/**
 * GET /companies/:id
 * Эндпоинт получения данных компании.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function getOne(req, res) {
  logger.init("get company");
  const { id } = req.params;
  const parsedId = parseInt(id);

  const [company, readError] = await handlePromise(companyMethods.getOne(id));
  if (readError) {
    throw new InternalError();
  }

  if (!company) {
    throw new NotFound("Contact not found");
  }

  const photoUrl = getUrlForRequest(req);
  res.status(OK).json(parseOne(company, photoUrl));
  logger.success();
}

module.exports = {
  getOne,
};
