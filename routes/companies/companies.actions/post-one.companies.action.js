const logger = require("../../../services/logger.service")(module);
const { OK } = require("../../../constants/http-codes");
const companyMethods = require("../../../DB/postgres/methods/company");
const { getUrlForRequest } = require("../../../helpers/url.helper");
const { NotFound, InternalError } = require("../../../constants/errors");
const { parseOne } = require("../companies.service");
const { handlePromise } = require("../../../utils/promiseHandler");

/**
 * GET /companies/:id
 * Эндпоинт получения данных компании.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function postOne(req, res) {
  logger.init("create company");
  const {id: userId} = req.payload;

  const company = {
    name: req.body.name,
    short_name: req.body.short_name,
    business_entity: req.body.business_entity,
    status: req.body.status,
    type: JSON.stringify(req.body.type),
    contract: JSON.stringify(req.body.contract),
    contact_id: req.body.contact,
    address: req.body.address
  }

  const [createdCompany, insertError] = await handlePromise(companyMethods.postOne(company, userId));
  if (insertError) {
    throw new InternalError();
  }

  res.status(OK).json(createdCompany);
  logger.success();
}

module.exports = {
  postOne,
};
