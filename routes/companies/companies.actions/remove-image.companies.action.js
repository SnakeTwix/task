const path = require("path");
const logger = require("../../../services/logger.service")(module);
const { OK } = require("../../../constants/http-codes");
const imagesConfig = require("../../../config").images;
const imageService = require("../../../services/image.service");
const { NotFound, InternalError, Unauthorized } = require("../../../constants/errors");
const companyMethods = require("../../../DB/postgres/methods/company");
const { handlePromise } = require("../../../utils/promiseHandler");
const { internalError } = require("../../../config/swagger/common-errors");

/**
 * DELETE /companies/:id/image
 * Эндпоинт удаления изображения компании.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function removeImage(req, res) {
  logger.init("remove company image");
  const { id } = req.params;
  const parsedId = parseInt(id);
  const { id: userId } = req.payload;
  const parsedUserId = parseInt(userId);
  const { image_name: fileName } = req.query;

  // Check if company belongs to user
  const [companyBelongsToUser, dbError] = await handlePromise(companyMethods.checkUser(parsedId, parsedUserId));
  if (dbError) {
    throw new InternalError()
  }

  if (!companyBelongsToUser) {
    throw new Unauthorized("Can't edit this company")
  }

  const [company, readError] = await handlePromise(companyMethods.getOne(parsedId));
  if (readError) {
    throw new InternalError();
  }

  if (!company) {
    throw new NotFound("Company not found");
  }

  const filePath = path.resolve(
    `${imagesConfig.imagesDir}/${userId}/${fileName}`
  );
  const [_image, imageDeleteError] = await handlePromise(imageService.removeImage(filePath));
  if (imageDeleteError) {
    console.log(imageDeleteError)
    throw new NotFound()
  }

  const fileExtension = path.extname(fileName).toLowerCase();
  const _fileName = fileName.split(".").slice(0, -1).join(".");
  const thumbName = `${_fileName}_${imagesConfig.thumbSize}x${imagesConfig.thumbSize}${fileExtension}`;
  const thumbPath = path.resolve(
    `${imagesConfig.imagesDir}${userId}/${thumbName}`
  );

  const [_thumb, thumbDeleteError] = await handlePromise(imageService.removeImage(thumbPath));
  if (thumbDeleteError) {
    throw new NotFound();
  }

  const [_, removeError] = await handlePromise(companyMethods.removeImage(parsedId, company.photos, fileName));
  if (removeError) {
    throw new InternalError();
  }

  res.status(OK).json();
  logger.success();
}

module.exports = {
  removeImage,
};
