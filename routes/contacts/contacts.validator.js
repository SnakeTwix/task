const { check, body, param } = require("express-validator");
const { UnprocessableEntity } = require("../../constants/errors");
const validate = require("../../middleware/validation.middleware");

const getOne = [
  check("id").isNumeric().withMessage({
    code: UnprocessableEntity,
    message: "id: parameter has incorrect format",
  }),
  validate,
];

const editOne = [
  check("id").isNumeric().withMessage({
    code: UnprocessableEntity,
    message: "id: parameter has incorrect format",
  }),
  validate,
];

const postOne = [
  body("lastname")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "lastname: required"
    }),
  body("firstname")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "firstname: required"
    }),
  body("patronymic")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "patronymic: required"
    }),
  body("phone")
    .isString()
    .withMessage({
      code: UnprocessableEntity,
      message: "phone: required"
    }),
  body("email")
    .isEmail()
    .withMessage({
      code: UnprocessableEntity,
      message: "email: should be email"
    }),
  validate
]

const removeOne = [
  param("id")
    .isNumeric()
    .withMessage({
      code: UnprocessableEntity,
      message: "id: parameter has incorrect format",
    }),
  validate
]

module.exports = { getOne, editOne, postOne, removeOne };
