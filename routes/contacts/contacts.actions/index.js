module.exports = {
  ...require("./get-one.contacts.action"),
  ...require("./edit-one.contacts.action"),
  ...require("./post-one.contacts.action"),
  ...require("./remove-one.contacts.action"),
};
