const logger = require("../../../services/logger.service")(module);
const { OK } = require("../../../constants/http-codes");
const contactMethods = require("../../../DB/postgres/methods/contact");
const { NotFound, InternalError } = require("../../../constants/errors");
const { handlePromise } = require("../../../utils/promiseHandler");

/**
 * GET /contacts/:id
 * Эндпоинт получения данных контакта.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function getOne(req, res) {
  logger.init("get contact");
  const { id } = req.params;
  const parsedId = parseInt(id);

  const [contact, readError] = await handlePromise(contactMethods.getOne(parsedId));
  if (readError) {
    throw new InternalError();
  }

  if (!contact) {
    throw new NotFound("Contact not found");
  }

  res.status(OK).json(contact);
  logger.success();
}

module.exports = {
  getOne,
};
