const logger = require("../../../services/logger.service")(module);
const { OK } = require("../../../constants/http-codes");
const contactMethods = require("../../../DB/postgres/methods/contact");
const { NotFound, InternalError } = require("../../../constants/errors");
const { handlePromise } = require("../../../utils/promiseHandler");

/**
 * GET /companies/:id
 * Эндпоинт получения данных компании.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function postOne(req, res) {
  logger.init("create company");

  const contacts = {
    lastname: req.body.lastname,
    firstname: req.body.firstname,
    patronymic: req.body.patronymic,
    phone: req.body.phone,
    email: req.body.email,
  }

  const [createdCompany, insertError] = await handlePromise(contactMethods.postOne(contacts));
  if (insertError) {
    throw new InternalError();
  }

  res.status(OK).json(createdCompany);
  logger.success();
}

module.exports = {
  postOne,
};
