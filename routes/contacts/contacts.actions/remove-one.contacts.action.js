const logger = require("../../../services/logger.service")(module);
const { OK } = require("../../../constants/http-codes");
const contactMethods = require("../../../DB/postgres/methods/contact");
const { NotFound, InternalError, BadRequest } = require("../../../constants/errors");
const { handlePromise } = require("../../../utils/promiseHandler");

/**
 * GET /contacts/:id
 * Эндпоинт получения данных контакта.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function removeOne(req, res) {
  logger.init("get contact");
  const { id } = req.params;
  const parsedId = parseInt(id);

  const [company, companyReadError] = await handlePromise(contactMethods.getCompany(parsedId));
  if (companyReadError) {
    throw  new InternalError()
  }

  if (company) {
    throw new BadRequest("Can't delete contact, because it's bound to a company")
  }

  const [contact, removeError] = await handlePromise(contactMethods.removeOne(parsedId));
  if (removeError) {
    throw new InternalError();
  }

  if (!contact) {
    throw new NotFound("Contact not found");
  }

  res.status(OK).json(contact);
  logger.success();
}

module.exports = {
  removeOne,
};
