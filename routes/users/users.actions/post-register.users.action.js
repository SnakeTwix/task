const { OK, INTERNAL_ERROR } = require("../../../constants/http-codes");
const userMethods = require("../../../DB/postgres/methods/users");
const logger = require("../../../services/logger.service")(module);
const bcrypt = require("bcryptjs");
const { handlePromise } = require("../../../utils/promiseHandler");
const { internalError } = require("../../../config/swagger/common-errors");
const JwtService = require("../../../services/jwt.service");
const { jwt: jwtConfig } = require("../../../config");

/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 * @returns {Promise<void>}
 */
async function postRegister(req, res) {
  logger.init("post user register");
  const user = req.body;

  const [hashedPassword, passwordError] = await handlePromise(bcrypt.hash(user.password, 10));
  if (passwordError) {
    res.sendStatus(INTERNAL_ERROR);
    return logger.error(passwordError);
  }
  user.password = hashedPassword;

  const [insertedUser, insertError] = await handlePromise(userMethods.postRegister(user));
  if (insertError) {
    res.sendStatus(INTERNAL_ERROR);
    return logger.error(insertError);
  }

  const token = new JwtService(jwtConfig).encode(insertedUser).data;
  res.header("Authorization", `Bearer ${token}`);
  res.sendStatus(OK);

  logger.success();
}

module.exports = {
  postRegister,
};
