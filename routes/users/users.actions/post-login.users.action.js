const { OK, INTERNAL_ERROR, NOT_FOUND, BAD_REQUEST, UNAUTHORIZED } = require("../../../constants/http-codes");
const userMethods = require("../../../DB/postgres/methods/users");
const logger = require("../../../services/logger.service")(module);
const bcrypt = require("bcryptjs");
const { handlePromise } = require("../../../utils/promiseHandler");
const { internalError } = require("../../../config/swagger/common-errors");
const JwtService = require("../../../services/jwt.service");
const { jwt: jwtConfig } = require("../../../config");

/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 * @returns {Promise<void>}
 */
async function postLogin(req, res) {
  logger.init("post user login");
  const user = req.body;

  const [fetchedUser, readError] = await handlePromise(userMethods.postLogin(user.username));
  if (readError) {
    res.sendStatus(INTERNAL_ERROR);
    return logger.error(readError);
  }

  if (!fetchedUser) {
    res.sendStatus(UNAUTHORIZED);
    return logger.success();
  }

  const [same, bcryptError] = await handlePromise(bcrypt.compare(user.password, fetchedUser.password));
  if (bcryptError) {
    res.sendStatus(INTERNAL_ERROR);
    return logger.error(readError);
  }

  if (!same) {
    res.sendStatus(UNAUTHORIZED);
    return logger.success();
  }

  const token = new JwtService(jwtConfig).encode({id: fetchedUser.id, username: fetchedUser.username}).data;
  res.header("Authorization", `Bearer ${token}`);

  res.sendStatus(OK);
  logger.success();
}

module.exports = {
  postLogin,
};
