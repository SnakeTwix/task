const { check, body } = require("express-validator");
const { UnprocessableEntity } = require("../../constants/errors");
const validate = require("../../middleware/validation.middleware");

const getAuth = [
  check("user")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "user: parameter is required",
    })
    .bail()
    .custom((value) => value.id)
    .withMessage({
      code: UnprocessableEntity,
      message: "user.id: parameter is required",
    }),
  validate,
];

const postRegister = [
    body("username")
      .notEmpty()
      .withMessage({
        code: UnprocessableEntity,
        message: "username: required"
      }),
  body("password")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "password: required"
    })
    .bail()
    .isLength({
      min: 5
    })
    .withMessage({
      code: UnprocessableEntity,
      message: "password: should be at least 5 characters"
    }),
  validate
]

const postLogin = [
  body("username")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "username: required"
    }),
  body("password")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "password: required"
    }),
  validate
]

module.exports = { getAuth, postRegister, postLogin };
