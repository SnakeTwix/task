FROM node:20-alpine as dev
WORKDIR /app
COPY . .

RUN npm install
EXPOSE 2114

CMD ["npm", "run", "start-dev"]