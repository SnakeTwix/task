const { postgres } = require("../../services/database.service");

/**
 *
 * @returns {import("knex").Knex}
 */
function connectionFactory() {
  const { connection } = postgres;
  return connection;
}

module.exports = connectionFactory;
