const knex = require("knex");
const {env} = require('../../../config/config.global')

/**
 *
 * @param {knex.Knex} knex
 * @returns {Promise<*>}
 */
exports.up = async (knex) => {
  // Table definitions
  await knex.schema
    .createTable("users", (table) => {
      table.increments("id", { primaryKey: true });
      table.string("username", 100).unique();
      table.string("password", 1024);
      table.datetime("created_at").defaultTo(knex.fn.now());
      table.datetime("updated_at").defaultTo(knex.fn.now());
    })
    .createTable("contacts", (table) => {
      table.increments("id", { primaryKey: true });
      table.string("firstname", 100);
      table.string("lastname", 100);
      table.string("patronymic", 100);
      table.string("phone", 16);
      table.string("email", 100)
      table.datetime("created_at").defaultTo(knex.fn.now());
      table.datetime("updated_at").defaultTo(knex.fn.now());
    })
    .createTable("companies", (table) => {
      table.increments("id", { primaryKey: true });
      table.string("name", 100);
      table.string("short_name", 100);
      table.string("business_entity", 100);
      table.string("status", 100);
      table.jsonb("contract").defaultTo("{}");
      table.jsonb("type").defaultTo("[]");
      table.jsonb("photos").defaultTo("[]");
      table.string("address", 255)
      table.integer("contact_id").unsigned().references("contacts.id");
      table.datetime("created_at").defaultTo(knex.fn.now());
      table.datetime("updated_at").defaultTo(knex.fn.now());
    })
    .createTable("user_companies", (table) => {
      table.integer("user_id").unsigned().references("users.id");
      table.integer("company_id").unsigned().references("companies.id");
    })


  // If we're in production, don't need the db dump loaded
  if (!env.dev) {
    return;
  }

  // DB Dump loading
  const dbDumpLocation = "../../../db_dump"
  const users = require(`${dbDumpLocation}/users.json`)
  const contacts = require(`${dbDumpLocation}/contacts.json`)
  const companies = require(`${dbDumpLocation}/companies.json`)
  const userCompanies = require(`${dbDumpLocation}/user_companies.json`)


  // console.log(companies)

  await knex.insert(users).into("users");
  await knex.insert(contacts).into("contacts");
  await knex.insert(companies).into("companies");
  await knex.insert(userCompanies).into("user_companies");
};

/**
 *
 * @param {knex.Knex} knex
 * @returns {Promise<*>}
 */
exports.down = async (knex) => {
  return knex.schema
    .dropTableIfExists("user_companies")
    .dropTableIfExists("companies")
    .dropTableIfExists("contacts")
    .dropTableIfExists("users");
};
