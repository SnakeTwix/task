const connection = require("../../index");

/**
 * Возвращает данные компании с указанным идентификатором.
 * @param {number} id
 */
async function getCompany(id) {
  const pg = connection();

  return (await pg.select("*").from("companies").where({contact_id: id}))[0];
}

module.exports = { getCompany };
