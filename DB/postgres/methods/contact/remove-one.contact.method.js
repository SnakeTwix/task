const connection = require('../../index')


// const mock = {
//   id: 16,
//   lastname: "Григорьев",
//   firstname: "Сергей",
//   patronymic: "Петрович",
//   phone: "79162165588",
//   email: "grigoriev@funeral.com",
//   createdAt: "2020-11-21T08:03:26.589Z",
//   updatedAt: "2020-11-23T09:30:00Z",
// };


/**
 * Возвращает данные контакта с указанным идентификатором.
 * @param {number} id
 */
async function removeOne(id) {
    const pg = connection();

    return (await pg.delete("*").from("contacts").where({id}))[0];
}

module.exports = { removeOne };
