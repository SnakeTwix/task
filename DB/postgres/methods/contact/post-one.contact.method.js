const connection = require("../../index");

/**
 * Возвращает данные компании с указанным идентификатором.
 * @param {object} contact
 */
async function postOne(contact) {
  const pg = connection();

  const createdContact = (await pg.insert(contact).into("contacts").returning("*"))[0];
  return createdContact;
}

module.exports = { postOne };
