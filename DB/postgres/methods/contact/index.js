module.exports = {
  ...require("./get-one.contact.method"),
  ...require("./edit-one.contact.method"),
  ...require("./post-one.contact.method"),
  ...require("./remove-one.contact.method"),
  ...require("./get-company.contact.method"),
};
