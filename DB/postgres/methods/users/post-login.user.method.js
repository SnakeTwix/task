const connection = require('../../index')

/**
 * Creates a user
 * @param {string} username
 */
async function postLogin(username) {
  const pg = connection();

  return (await pg.select("id", "username", "password").from("users").where({ username }).limit(1))[0]
}

module.exports = { postLogin };
