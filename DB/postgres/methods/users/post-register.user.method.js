const connection = require('../../index')

/**
 * Creates a user
 * @param {Object} user
 */
async function postRegister(user) {
  const pg = connection();

  
  return (await pg.insert(user).into("users").returning(['id', 'username']))[0]
}

module.exports = { postRegister };
