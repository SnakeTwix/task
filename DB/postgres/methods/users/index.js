module.exports = {
  ...require("./post-register.user.method"),
  ...require("./post-login.user.method"),
};
