const connection = require("../../index");

/**
 * Возвращает данные компании с указанным идентификатором.
 * @param {number} id
 * @param {number} userId
 */
async function checkUser(id, userId) {
  const pg = connection();
  return (await pg.select("*").from("user_companies").where({user_id: userId, company_id: id})).length !== 0;
}

module.exports = { checkUser };
