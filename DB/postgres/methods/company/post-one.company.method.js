const connection = require("../../index");

/**
 * Возвращает данные компании с указанным идентификатором.
 * @param {object} company
 * @param {number} userId
 */
async function postOne(company, userId) {
  const pg = connection();

  const createdCompany = (await pg.insert(company).into("companies").returning("*"))[0];
  await pg.insert({user_id: userId, company_id: createdCompany.id}).into("user_companies");

  return createdCompany
}

module.exports = { postOne };
