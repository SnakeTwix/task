const connection = require("../../index");

/**
 * Возвращает данные компании с указанным идентификатором.
 * @param {number} id
 * @param {Object[] | null} existingImages
 * @param {string} imageName
 */
async function removeImage(id, existingImages, imageName) {
  const pg = connection();
  if (!Array.isArray(existingImages)) {
    return;
  }

  existingImages = existingImages.filter(images => images.name !== imageName);
  await pg.update({ photos: JSON.stringify(existingImages) }).into("companies").where({id});
}

module.exports = { removeImage };
