const connection = require("../../index");

/**
 * Returns companies
 * @param {number} limit
 * @param {number} offset
 * @param {string} status
 * @param {string} companyType
 */
async function getMany(limit, offset, status = '', companyType) {
  const pg = connection();
  let query = pg.select("*").from("companies").whereILike("status", `%${status}%`);

  if (companyType) {
    query.andWhereRaw("? IN (SELECT jsonb_array_elements(type))", [`"${companyType}"`]);
  }

  return query.orderBy("id").limit(limit).offset(offset);
}

module.exports = { getMany };
