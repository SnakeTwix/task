module.exports = {
  ...require("./get-one.company.method"),
  ...require("./edit-one.company.method"),
  ...require("./add-image.company.method"),
  ...require("./remove-image.company.method"),
  ...require("./check-user.company.method"),
  ...require("./post-one.company.method"),
  ...require("./get-many.company.method"),
};
