const connection = require("../../index");

/**
 * Возвращает данные компании с указанным идентификатором.
 * @param {number} id
 * @param {Object[] | null} existingImages
 * @param {Object} uploadedImage
 */
async function addImage(id, existingImages, uploadedImage) {
  const pg = connection();
  if (!Array.isArray(existingImages)) {
    existingImages = [];
  }
  existingImages.push(uploadedImage);
  await pg.update({ photos: JSON.stringify(existingImages) }).into("companies").where({id});
}

module.exports = { addImage };
