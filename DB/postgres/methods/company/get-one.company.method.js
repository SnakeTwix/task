const connection = require("../../index");

/**
 * Возвращает данные компании с указанным идентификатором.
 * @param {number} id
 */
async function getOne(id) {
  const pg = connection();

  return (await pg.select("*").from("companies").where({id}))[0];
}

module.exports = { getOne };
