const { dbs: dbsConfig, env } = require("../config");
const knex = require("knex")
const logger = require("./logger.service")(module);

/**
 * Базовый класс сервиса работы с базой данных
 */
class Database {

  // Construct URI instead of passing it in
  #type; // Used DB name
  #host; // Database connection host
  #port;
  #user;
  #password;
  #database;

  /**
   * @type {knex.Knex} #connection
   */
  #connection;


  constructor(config) {
    // Ideally we'd have no base class and just let this be a Postgres handling class, but eh
    this.#type = "postgres";
    this.#host = config.host;
    this.#port = config.port;
    this.#user = config.username;
    this.#password = config.password;
    this.#database = config.database;
  }

  /**
   * Открывает соединение с БД.
   * @return {Promise<void>}
   */
  async connect() {
    try {
      const connection = await knex({
        client: this.#type,
        connection: {
          host: this.#host,
          port: this.#port,
          user: this.#user,
          password: this.#password,
          database: this.#database
        }
      })

      this.#connection = connection;
      logger.info(`Connected to ${this.#type}`);
    } catch (error) {
      logger.error(`Unable to connect to ${this.#type}:`, error.message);
    }
  }

  /**
   * Runs migrations if the connection exists
   * @return {Promise<void>}
   */
  async runMigrations() {
    if (!this.#connection) {
      logger.error("Trying to run migrations without an existing connection");
      return;
    }

    try {
      await this.#connection.migrate.latest({
        directory: `${__dirname}/../DB/${this.#type}/migrations`
      })

      logger.info("Ran migrations")
    } catch (error) {
      logger.error(`Unable to run migrations:`, error)
    }
  }

  /**
   * Закрывает соединение с БД.
   * @return {Promise<void>}
   */
  async disconnect() {
    if (this.#connection) {
      try {
        // todo: метод закрытия соединения с БД
        await this.#connection.destroy();
        logger.info(`Disconnected from ${this.#type}`);
      } catch (error) {
        logger.error(`Unable to disconnect from ${this.#type}:`, error.message);
      }
    }
  }

  /**
   * Возвращает объект соединения с БД,
   * @return {knex.Knex}
   */
  get connection() {
    return this.#connection;
  }
}

const postgres = new Database(dbsConfig.postgres);

module.exports = { postgres };
